O programa tem por objetivo demonstrar como é feita a Alocação de memória utilizando os algoritimos:

Best-fit:
Escolhe a melhor partição, ou seja, aquela que o programa deixa o menor espaço sem utilização.
Lista de áreas livres alocada por tamanho, diminuindo o tempo de busca
Desvantagem de deixar pequenas áreas não contíguas, aumentando o problema da fragmentação.

Worst-fit:
Escolhe a pior partição, ou seja, aquela que o programa deixa o maior espaço sem utilização.
Diminui o problema de fragmentação, deixando espaços livres maiores que permitem a um maior número de programas utilizar a memória.

First-fit:
Escolhe a primeira partição livre de tamanho suficiente para carregar o programa
Lista de áreas livres ordenada por endereços crescentemente.
Grande chance de se obter uma grande partição livre nos endereços de memórias mais altos.
Mais rápida e consome menos recursos do sistema.


Irá rodar em sistemas operacionais Windows com .NET Framework 4.5.2 ou superior.
O projeto está hospedado em https://gitlab.com/Vivannaboa/AlocacaoDeMemoria.git
Arquivo de instação Setup.exe.
Arquivo com o fonte principal FormExecucao.cs
