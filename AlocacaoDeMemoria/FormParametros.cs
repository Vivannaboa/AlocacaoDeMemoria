﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlocacaoDeMemoria
{
    public partial class FormParametros : Form
    {
        public string Algoritimo { get; set; }
        public int NumeroDeProcessos { get; set; }
        public float Tempo { get; set; }

        public FormParametros()
        {
            InitializeComponent();

        }

        private void numeroDeProcessos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }
        
        private void btnExecutar_Click(object sender, EventArgs e)
        {
            //valida algoritimo
            if (comboBoxAlgoritimo.SelectedItem==null)
            {
                MessageBox.Show("O Algoritimmo deve ser selecionado!");
                comboBoxAlgoritimo.Focus();
                return;
            }
            if (textNumeroDeProcessos.Text == "")
            {
                MessageBox.Show("O número de processos deve ser informado!");
                textNumeroDeProcessos.Focus();
                return;
            }

            this.Algoritimo = comboBoxAlgoritimo.SelectedItem.ToString();
            this.NumeroDeProcessos = int.Parse(textNumeroDeProcessos.Text);
            if (textTempo.Text == "")
            {
                this.Tempo = 0;
            }else{
                this.Tempo = float.Parse(textTempo.Text);
            }

            this.DialogResult = DialogResult.OK;
            //Close();     

        }
    }
}
