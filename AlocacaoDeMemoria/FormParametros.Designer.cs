﻿namespace AlocacaoDeMemoria
{
    partial class FormParametros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxAlgoritimo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textNumeroDeProcessos = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textTempo = new System.Windows.Forms.TextBox();
            this.btnExecutar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Algoritimo";
            // 
            // comboBoxAlgoritimo
            // 
            this.comboBoxAlgoritimo.FormattingEnabled = true;
            this.comboBoxAlgoritimo.Items.AddRange(new object[] {
            "First-fit",
            "Circular-fit",
            "Best-fit",
            "Worst-fit"});
            this.comboBoxAlgoritimo.Location = new System.Drawing.Point(57, 49);
            this.comboBoxAlgoritimo.Name = "comboBoxAlgoritimo";
            this.comboBoxAlgoritimo.Size = new System.Drawing.Size(293, 21);
            this.comboBoxAlgoritimo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Número de processos";
            // 
            // textNumeroDeProcessos
            // 
            this.textNumeroDeProcessos.Location = new System.Drawing.Point(57, 93);
            this.textNumeroDeProcessos.Name = "textNumeroDeProcessos";
            this.textNumeroDeProcessos.Size = new System.Drawing.Size(293, 20);
            this.textNumeroDeProcessos.TabIndex = 3;
            this.textNumeroDeProcessos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numeroDeProcessos_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tempo";
            // 
            // textTempo
            // 
            this.textTempo.Location = new System.Drawing.Point(57, 136);
            this.textTempo.Name = "textTempo";
            this.textTempo.Size = new System.Drawing.Size(293, 20);
            this.textTempo.TabIndex = 5;
            // 
            // btnExecutar
            // 
            this.btnExecutar.Location = new System.Drawing.Point(57, 163);
            this.btnExecutar.Name = "btnExecutar";
            this.btnExecutar.Size = new System.Drawing.Size(293, 23);
            this.btnExecutar.TabIndex = 6;
            this.btnExecutar.Text = "Executar";
            this.btnExecutar.UseVisualStyleBackColor = true;
            this.btnExecutar.Click += new System.EventHandler(this.btnExecutar_Click);
            // 
            // FormParametros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 233);
            this.Controls.Add(this.btnExecutar);
            this.Controls.Add(this.textTempo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textNumeroDeProcessos);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxAlgoritimo);
            this.Controls.Add(this.label1);
            this.Name = "FormParametros";
            this.Text = "FormParametros";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxAlgoritimo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textNumeroDeProcessos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textTempo;
        private System.Windows.Forms.Button btnExecutar;
    }
}