﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlocacaoDeMemoria.Entidades
{
    public class Processo
    {
        public int Pid { get; set; }
        public Brush Cor { get; set; }
        public int Tamanho { get; set; }
        public float PontoInicio { get; set; }
        public float PontoFim { get; set; }
        public int TempoEmExecucao { get; set; }
        public int Espera { get; set; }
        
    }
}
