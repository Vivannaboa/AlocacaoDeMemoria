﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlocacaoDeMemoria.Entidades
{
   public class Ponto
    {
        public int Id { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Width { get; set; }
        public float Heigth { get; set; }
        public bool Pintado { get; set; }
        public Brush brush { get; set; }
    }
}
