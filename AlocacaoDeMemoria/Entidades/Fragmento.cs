﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlocacaoDeMemoria.Entidades
{
    public class Fragmento
    {
        public int Inicio { get; set; }
        public int Fim { get; set; }
        public int Tamanho { get; set; }
    }
}
