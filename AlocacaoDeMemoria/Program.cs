﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlocacaoDeMemoria
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            FormParametros frm = new FormParametros();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new FormExecucao(frm.Algoritimo, frm.NumeroDeProcessos, frm.Tempo));
            }

        }
    }
}
