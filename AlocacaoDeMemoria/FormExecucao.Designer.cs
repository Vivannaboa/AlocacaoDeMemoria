﻿using System.Drawing;

namespace AlocacaoDeMemoria
{
    partial class FormExecucao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listViewLegend = new System.Windows.Forms.ListView();
            this.Processo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CollTempo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.collTamanho = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.lblAlgoritimo = new System.Windows.Forms.Label();
            this.lblMemoriaDisponivel = new System.Windows.Forms.Label();
            this.listViewRelatorio = new System.Windows.Forms.ListView();
            this.collNome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.collValor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnReabrir = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(465, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(273, 565);
            this.panel1.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.listViewLegend);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(267, 491);
            this.panel3.TabIndex = 1;
            // 
            // listViewLegend
            // 
            this.listViewLegend.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Processo,
            this.CollTempo,
            this.collTamanho});
            this.listViewLegend.Location = new System.Drawing.Point(3, 0);
            this.listViewLegend.Name = "listViewLegend";
            this.listViewLegend.Size = new System.Drawing.Size(255, 491);
            this.listViewLegend.TabIndex = 0;
            this.listViewLegend.UseCompatibleStateImageBehavior = false;
            this.listViewLegend.View = System.Windows.Forms.View.Details;
            // 
            // Processo
            // 
            this.Processo.Text = "Processo";
            this.Processo.Width = 76;
            // 
            // CollTempo
            // 
            this.CollTempo.Text = "Tempo";
            // 
            // collTamanho
            // 
            this.collTamanho.Text = "Tamanho";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.lblAlgoritimo);
            this.panel2.Controls.Add(this.lblMemoriaDisponivel);
            this.panel2.Location = new System.Drawing.Point(0, 500);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(270, 62);
            this.panel2.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Location = new System.Drawing.Point(0, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(270, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Desfragmentar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblAlgoritimo
            // 
            this.lblAlgoritimo.AutoSize = true;
            this.lblAlgoritimo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblAlgoritimo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlgoritimo.Location = new System.Drawing.Point(0, 17);
            this.lblAlgoritimo.Name = "lblAlgoritimo";
            this.lblAlgoritimo.Size = new System.Drawing.Size(46, 17);
            this.lblAlgoritimo.TabIndex = 2;
            this.lblAlgoritimo.Text = "label1";
            // 
            // lblMemoriaDisponivel
            // 
            this.lblMemoriaDisponivel.AutoSize = true;
            this.lblMemoriaDisponivel.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMemoriaDisponivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMemoriaDisponivel.Location = new System.Drawing.Point(0, 0);
            this.lblMemoriaDisponivel.Name = "lblMemoriaDisponivel";
            this.lblMemoriaDisponivel.Size = new System.Drawing.Size(131, 17);
            this.lblMemoriaDisponivel.TabIndex = 1;
            this.lblMemoriaDisponivel.Text = "Memória Disponivel";
            this.lblMemoriaDisponivel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // listViewRelatorio
            // 
            this.listViewRelatorio.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.collNome,
            this.collValor});
            this.listViewRelatorio.Dock = System.Windows.Forms.DockStyle.Top;
            this.listViewRelatorio.Location = new System.Drawing.Point(0, 0);
            this.listViewRelatorio.Name = "listViewRelatorio";
            this.listViewRelatorio.Size = new System.Drawing.Size(465, 534);
            this.listViewRelatorio.TabIndex = 2;
            this.listViewRelatorio.UseCompatibleStateImageBehavior = false;
            this.listViewRelatorio.View = System.Windows.Forms.View.Details;
            this.listViewRelatorio.Visible = false;
            // 
            // collNome
            // 
            this.collNome.Text = "Descrição";
            this.collNome.Width = 300;
            // 
            // collValor
            // 
            this.collValor.Text = "Valor";
            this.collValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.collValor.Width = 200;
            // 
            // btnReabrir
            // 
            this.btnReabrir.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReabrir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReabrir.Location = new System.Drawing.Point(0, 534);
            this.btnReabrir.Name = "btnReabrir";
            this.btnReabrir.Size = new System.Drawing.Size(465, 41);
            this.btnReabrir.TabIndex = 3;
            this.btnReabrir.Text = "Executar Novamente";
            this.btnReabrir.UseVisualStyleBackColor = true;
            this.btnReabrir.Visible = false;
            this.btnReabrir.Click += new System.EventHandler(this.btnReabrir_Click);
            // 
            // FormExecucao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(738, 565);
            this.Controls.Add(this.btnReabrir);
            this.Controls.Add(this.listViewRelatorio);
            this.Controls.Add(this.panel1);
            this.Name = "FormExecucao";
            this.Text = "Alocação de Memória";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormExecucao_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

       #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblMemoriaDisponivel;
        private System.Windows.Forms.ListView listViewLegend;
        private System.Windows.Forms.ColumnHeader Processo;
        private System.Windows.Forms.ColumnHeader CollTempo;
        private System.Windows.Forms.ColumnHeader collTamanho;
        private System.Windows.Forms.Label lblAlgoritimo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView listViewRelatorio;
        private System.Windows.Forms.ColumnHeader collNome;
        private System.Windows.Forms.ColumnHeader collValor;
        private System.Windows.Forms.Button btnReabrir;
    }
}

