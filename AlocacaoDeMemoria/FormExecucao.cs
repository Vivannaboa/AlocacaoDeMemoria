﻿using AlocacaoDeMemoria.Entidades;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlocacaoDeMemoria
{
    public partial class FormExecucao : Form
    {

        public string Algoritimo { get; set; }
        public int NumeroDeProcessos { get; set; }
        public float Tempo { get; set; }
        public int Pid { get; set; }
        public long TotalDeCiclos { get; set; }
        public long TotalDememoriaOcupadaPorCiclo { get; set; }
        public long TotalDememoriaLivrePorCiclo { get; set; }

        Fragmento ultimoFragmento = new Fragmento();
        public List<Processo> QuqProcessos = new List<Processo>();
        public Queue<Processo> QuqProcessoAguarando = new Queue<Processo>();
        public Queue<Processo> QuqProcessoFinalizados = new Queue<Processo>();
        public List<Ponto> ListPontos = new List<Ponto>();
        public List<Fragmento> Listfragmento = new List<Fragmento>();

        public Thread MyThread;
        public Graphics g;
        public ListViewItem lvi;
        public ListViewItem lvr;
        public Random r = new Random();

        public FormExecucao(string algoritimo, int numeroDeProcessos, float tempo)
        {
            //recebe os parametros da tela de parametros
            this.Algoritimo = algoritimo;
            this.NumeroDeProcessos = numeroDeProcessos;
            this.Tempo = tempo;
            //inicializa os componentes da tela
            InitializeComponent();
            g = CreateGraphics();
            this.Show();
            lblAlgoritimo.Text = "Algoritimo - " + algoritimo;
            //cria uma matriz de pontos para representar a memória
            popularPontos();

            //aqui inicia uma thread - se executar tudo na thread principal a tela fica travada
            Control.CheckForIllegalCrossThreadCalls = false;
            MyThread = new Thread(new ThreadStart(MyThreadFunction));
            MyThread.Start();
        }

        //thread responsavel por executar os algoritimos
        public void MyThreadFunction()
        {
            //registra o número de ciclos
            
            //termina o programa quando todos os processos forem destruidos
            while (QuqProcessoFinalizados.Count < NumeroDeProcessos)
            {
                TotalDeCiclos++;
                //sorteia se é para criar um proocesso ou não
                if (r.Next(1, 101) <= 20 && Pid < NumeroDeProcessos)
                {
                    criarProcesso();
                }

                //se rem um processo aguardando vai verificar o algoritimo e tentar alocar esse processo
                if (QuqProcessoAguarando.Count > 0)
                {
                    switch (Algoritimo)
                    {
                        case "First-fit":
                            algoritimoFirstFit();
                            break;
                        case "Circular-fit":
                            algoritimoCircularFit();
                            break;
                        case "Best-fit":
                            algoritimoBestFit();
                            break;
                        case "Worst-fit":
                            algoritimoWorstFit();
                            break;
                        default:
                            break;
                    }

                }

                decrementaTempoEmExecucao();
                incrementaTempoEmEspera();
                finalizaProcessos();
                pintar();
                //Delay
                Thread.Sleep(TimeSpan.FromSeconds(Tempo));
            }
            Invoke(new Action(() => mostraRelatorio()));
            //finaliza a segunda thread
            MyThread.Abort();


        }

        private void mostraRelatorio()
        {
            ListPontos.Clear();

            panel1.Visible = false;
            listViewRelatorio.Visible = true;

            lvr = new ListViewItem("Processos criados: ");
            lvr.SubItems.Add(NumeroDeProcessos.ToString());
            listViewRelatorio.Items.Add(lvr);
            lvr.BackColor = Color.Aquamarine;

            lvr = new ListViewItem("Percentual de memória ocupada: ");
            lvr.SubItems.Add((((TotalDememoriaOcupadaPorCiclo / TotalDeCiclos) * 100) / 5000).ToString() + "%");
            listViewRelatorio.Items.Add(lvr);
           

            lvr = new ListViewItem("Percentual de memória livre: ");
            lvr.SubItems.Add((((TotalDememoriaLivrePorCiclo / TotalDeCiclos)*100)/5000).ToString() + "%");
            listViewRelatorio.Items.Add(lvr);
            lvr.BackColor = Color.Aquamarine;

            lvr = new ListViewItem("Tempo médio aguardando alocação: ");
            lvr.SubItems.Add(string.Format("{0:n2}", QuqProcessoFinalizados.Average(e => e.Espera)));
            listViewRelatorio.Items.Add(lvr);            

            listViewRelatorio.Show();
            btnReabrir.Visible = true;
        }

        private void incrementaTempoEmEspera()
        {
            foreach (var item in QuqProcessoAguarando)
            {
                item.Espera++;
            }
        }

        //função responsavel por atualizar a memória disponivel
        private int getMemoriaDisponivel()
        {
            //soma o tamanho de todos os fragmentos usando as funções do LINQ
            var memoriaLivre = Listfragmento.Sum(l => l.Tamanho);
            TotalDememoriaLivrePorCiclo += memoriaLivre;
            TotalDememoriaOcupadaPorCiclo += (5000 - memoriaLivre);
            //como é uma thread, nao da para mecher diretamente nos componentes da thread principal.
            //aqui  pedimos para a thread principal fazer isso por nós.
            Invoke(new Action(() =>
            {
                lblMemoriaDisponivel.Text = "Memória disponivel = " + memoriaLivre.ToString();
            }));

            return memoriaLivre;
        }

        //função responsavel por encontrar os fragmentos livres
        private void getFragmentos()
        {
            Listfragmento.Clear();
            bool f = false;
            int tamanho = 0;
            int inicio = 0;
            int fim = -1;
            foreach (var item in ListPontos)
            {
                if (!item.Pintado)
                {
                    f = false;
                    fim = 0;

                    if (inicio == 0)
                    {
                        inicio = item.Id;
                    }
                    tamanho++;
                }
                else
                {
                    f = true;
                }

                if (f || (ListPontos.IndexOf(item) == ListPontos.Count - 1))
                {
                    if (fim == 0)
                    {
                        if (ListPontos.IndexOf(item) == ListPontos.Count - 1)
                        {
                            fim = item.Id;
                        }
                        else
                        {
                            fim = item.Id - 1;
                        }
                        Fragmento fragmento = new Fragmento();
                        fragmento.Inicio = inicio;
                        fragmento.Fim = fim;
                        fragmento.Tamanho = tamanho;

                        Listfragmento.Add(fragmento);
                        inicio = 0;
                        tamanho = 0;
                    }

                }
            }

        }

        //função responssavel por criar processos
        private void criarProcesso()
        {
            Pid++;
            Processo p1 = new Processo();
            p1.Pid = Pid;
            p1.Cor = getCor();
            p1.Tamanho = r.Next(100, 1001);
            p1.TempoEmExecucao = r.Next(1, 301);
            QuqProcessoAguarando.Enqueue(p1);
        }

        //sorteio de corres brush
        private Brush getCor()
        {
            Color color = Color.FromArgb(r.Next(255), r.Next(255), r.Next(255));
            Brush brush = new SolidBrush(color);
            return brush;
        }

        //criação de uma matriz com 5000 ponntos
        private void popularPontos()
        {
            int h = 900;
            int w = 900;
            var valor = 90;
            var id = 0;
            for (int i = 0; i < 50; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    id++;
                    Ponto p = new Ponto();
                    p.brush = getCor();
                    p.Id = id;
                    p.X = j * h / valor;
                    p.Y = i * w / valor;
                    p.Width = h / valor;
                    p.Heigth = w / valor;
                    p.Pintado = false;
                    ListPontos.Add(p);
                }

            }
        }

        //desenha na tela os pontos alocados e os não alocados
        public void pintar()
        {
            foreach (var processo in QuqProcessos)
            {

                foreach (var p in ListPontos)
                {
                    if (p.Id >= processo.PontoInicio && p.Id <= processo.PontoFim)
                    {
                        p.Pintado = true;
                        g.FillRectangle(processo.Cor, p.X, p.Y, p.Width, p.Heigth);
                    }
                }
            }
            getFragmentos();
            getMemoriaDisponivel();
            foreach (var item in Listfragmento.ToList())
            {
                foreach (var p in ListPontos)
                {
                    if (p.Id >= item.Inicio && p.Id <= item.Fim)
                    {
                        g.FillRectangle(new SolidBrush(Color.White), p.X, p.Y, p.Width, p.Heigth);
                    }
                }
            }

        }

        private void algoritimoWorstFit()
        {
            //pega o primeiro processo aguardando na fila, sem remove-lo
            Processo itemProcesso = QuqProcessoAguarando.Peek();
            //filtra os fragmentos que podem alocar esse processo e ordena do Maior para o Menor
            List<Fragmento> listaComOsFragmentosPossiveis = Listfragmento.Where(e => e.Tamanho >= itemProcesso.Tamanho).OrderByDescending(e => e.Tamanho).ToList();
            if (listaComOsFragmentosPossiveis.Count() > 0)
            {
                //se tiver um fragmento que suporta o processo pega o primeiro dessa fila
                Fragmento itemFragmento = listaComOsFragmentosPossiveis.First();

                //armazena nesse processo onde ele deve ser alocado
                itemProcesso.PontoInicio = itemFragmento.Inicio;
                itemProcesso.PontoFim = itemFragmento.Inicio + itemProcesso.Tamanho;
                //tira o processo da fila de processos aguardando
                QuqProcessos.Add(QuqProcessoAguarando.Dequeue());
            }
        }

        private void algoritimoBestFit()
        {
            //pega o primeiro processo aguardando na fila, sem remove-lo
            Processo itemProcesso = QuqProcessoAguarando.Peek();
            //filtra os fragmentos que podem alocar esse processo e ordena do Menor para o Maior
            List<Fragmento> listaComOsFragmentosPossiveis = Listfragmento.Where(e => e.Tamanho >= itemProcesso.Tamanho).OrderBy(e => e.Tamanho).ToList();
            if (listaComOsFragmentosPossiveis.Count() > 0)
            {
                //se tiver um fragmento que suporta o processo pega o primeiro dessa fila
                Fragmento itemFragmento = listaComOsFragmentosPossiveis.First();

                //armazena nesse processo onde ele deve ser alocado
                itemProcesso.PontoInicio = itemFragmento.Inicio;
                itemProcesso.PontoFim = itemFragmento.Inicio + itemProcesso.Tamanho;
                //tira o processo da fila de processos aguardando
                QuqProcessos.Add(QuqProcessoAguarando.Dequeue());
            }

        }

        private void algoritimoCircularFit()
        {
            //pega o primeiro processo aguardando na fila, sem remove-lo
            Processo itemProcesso = QuqProcessoAguarando.Peek();
            //filtra os fragmentos que podem alocar esse processo e ordena pelo inicio
            List<Fragmento> ListaFragmentosPossiveis = Listfragmento.Where(e => e.Inicio >= ultimoFragmento.Inicio && e.Tamanho >= itemProcesso.Tamanho).OrderBy(e => e.Inicio).ToList();
            if (ListaFragmentosPossiveis.Count > 0)
            {
                //se tiver um fragmento que suporta o processo pega o primeiro dessa fila
                Fragmento itemFragmento = ListaFragmentosPossiveis.First();
                //armazena nesse processo onde ele deve ser alocado
                itemProcesso.PontoInicio = itemFragmento.Inicio;
                itemProcesso.PontoFim = itemFragmento.Inicio + itemProcesso.Tamanho;
                //tira o processo da fila de processos aguardando
                QuqProcessos.Add(QuqProcessoAguarando.Dequeue());
            }
            else
            {
                //se chegou ao final da memória e não conseguiu alocar volta para o primeiro
                ultimoFragmento.Inicio = 0;
            }
        }

        private void algoritimoFirstFit()
        {
            Processo itemProcesso = QuqProcessoAguarando.Peek();
            //filtra os fragmentos que podem alocar esse processo e ordena pelo inicio
            List<Fragmento> ListaFragmentosPossiveis = Listfragmento.Where(e => e.Tamanho >= itemProcesso.Tamanho).OrderBy(e => e.Inicio).ToList();
            if (ListaFragmentosPossiveis.Count > 0)
            {
                //se tiver um fragmento que suporta o processo pega o primeiro dessa fila
                Fragmento itemFragmento = ListaFragmentosPossiveis.First();
                //armazena nesse processo onde ele deve ser alocado
                itemProcesso.PontoInicio = itemFragmento.Inicio;
                itemProcesso.PontoFim = itemFragmento.Inicio + itemProcesso.Tamanho;
                //tira o processo da fila de processos aguardando
                QuqProcessos.Add(QuqProcessoAguarando.Dequeue());
            }
        }

        //muda de fila os processos que terminaram de executar
        private void finalizaProcessos()
        {
            foreach (var p in QuqProcessos.ToList())
            {
                if (p.TempoEmExecucao == 0)
                {
                    foreach (var pontosItemTerminado in ListPontos)
                    {
                        if (pontosItemTerminado.Id >= p.PontoInicio && pontosItemTerminado.Id <= p.PontoFim)
                        {
                            pontosItemTerminado.Pintado = false;
                        }
                    }
                    QuqProcessoFinalizados.Enqueue(p);
                    QuqProcessos.Remove(p);
                }
            }
        }

        //função responsavel por limpar a lista de processos e decrementar o tempo de execução.
        private void decrementaTempoEmExecucao()
        {
            //limpa a lista
            listViewLegend.Items.Clear();
            //atualiza os itens da lista
            foreach (var p in QuqProcessos)
            {
                //decrementa o tempo de execução do processo
                p.TempoEmExecucao -= 1;

                ListViewItem lvi = new ListViewItem("PID - " + p.Pid.ToString());
                lvi.SubItems.Add(p.TempoEmExecucao.ToString());
                lvi.SubItems.Add(p.Tamanho.ToString());
                listViewLegend.Items.Add(lvi);
                lvi.BackColor = new Pen(p.Cor).Color;
            }

            lvi = new ListViewItem("Agruardando");
            lvi.SubItems.Add("Aguardando");
            lvi.SubItems.Add("Aguardando");

            listViewLegend.Items.Add(lvi);
            lvi.BackColor = new Pen(Color.Black).Color;
            //mostra os fragmentos na lista também
            foreach (var p in Listfragmento.ToList())
            {
                lvi = new ListViewItem("Fragmento");
                lvi.SubItems.Add("Fragmento");
                lvi.SubItems.Add("Fragmento");
                lvi.SubItems.Add(p.Tamanho.ToString());

                listViewLegend.Items.Add(lvi);
                lvi.BackColor = new Pen(Color.White).Color;
            }
            //mostra os processos aguardando
            lvi = new ListViewItem("Agruardando");
            lvi.SubItems.Add("Aguardando");
            lvi.SubItems.Add("Aguardando");

            listViewLegend.Items.Add(lvi);
            lvi.BackColor = new Pen(Color.Black).Color;


            foreach (var item in QuqProcessoAguarando)
            {
                lvi = new ListViewItem("PID - " + item.Pid.ToString());
                lvi.SubItems.Add("Aguardando");
                lvi.SubItems.Add("").BackColor = new Pen(Color.White).Color;
                lvi.SubItems.Add(item.Tamanho.ToString());

                listViewLegend.Items.Add(lvi);
            }
        }

        //finalizar a thread antes de fechar se o usuário decidir finalizar antes de terminar a alocação
        private void FormExecucao_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            MyThread.Abort();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread.Sleep(1000);
            Listfragmento.Clear();
            float inicio = 0;
            foreach (var p in ListPontos)
            {
                p.Pintado = false;

            }
            foreach (var item in QuqProcessos.ToList())
            {
                item.PontoInicio = inicio;
                item.PontoFim = inicio + item.Tamanho;
                inicio = item.PontoFim + 1;
            }
        }

        private void btnReabrir_Click(object sender, EventArgs e)
        {
            //reinicia a aplicação
            Application.Restart();

            //fecha form actual
            this.Close();
        }
    }



}



